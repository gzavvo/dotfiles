#!/bin/bash
# sync_files is a script to sync my dotfiles/ backup. It also sync my Gitlab
# repo

# list of files to be sync
files=(".vimrc" ".vimrc.bepo" "xord-colortheme")

# TODO import COLORS
# TODO colorize output file names

sync_file() {
    echo "Syncing file: $1 ..."
    cp "${HOME}/$1" "${HOME}/Documents/dotfiles/$1"
    echo "cp "${HOME}/$1" "${HOME}/Documents/dotfiles/$1""
    echo
}

git_push() {
    git add 

# loop for all files in files
echo
echo "#------ BACKUP THE DOTFILES ------#"
echo

for file in "${files[@]}"; do

    
    # check if backup file is older than the original
    if [[ "${HOME}/Documents/dotfiles/${file}" -ot "${HOME}/${file}" ]]; then
        echo
        echo  "${file}.backup is older than the original"

        # ask for sync or not
        sync_answer=""
        while [[ ${sync_answer} != "y" && ${sync_answer} != "n" ]]; do
            echo "Do you want to sync files? (y/n)"
            read -p "> " sync_answer
        done
        
        # syncing
        if [[ ${sync_answer} = "y" ]]; then
            sync_file ${file}
        else
            :
        fi
    else
        echo "${file} already synced."
        echo
    fi
done
